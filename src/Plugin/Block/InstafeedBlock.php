<?php

namespace Drupal\instafeed_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;

/**
 * Provides a 'Instafeed Block' block.
 *
 * @Block(
 *   id = "instafeed_block",
 *   admin_label = @Translation("Instafeed Block"),
 * )
 */
class InstafeedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new InstafeedBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['help'] = [
      '#title' => $this->t('Help'),
      '#markup' => '<strong>To avoid unexpected behaviour, use only one Instafeed block per page.</strong>',
    ];

    // Ensure that the limit value is cast to an integer.
    $limitValue = isset($this->configuration['limit']) ? intval($this->configuration['limit']) : 6;

    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit posts'),
      '#default_value' => $limitValue,
      '#min' => 1,
      '#max' => 50,
      '#description' => $this->t('The number of Instagram posts to display (1 - 50).'),
    ];

    $form['filters'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter media types'),
      '#options' => [
        'image' => $this->t('Image'),
        'video' => $this->t('Video'),
        'album' => $this->t('Album'),
      ],
      '#default_value' => $this->configuration['filters'] ?? [],
      '#multiple' => TRUE,
      '#description' => $this->t('Select media types to include. If no types are selected, all available media types will be displayed.<br><strong>Limitations: The album type only displays the first image.</strong>'),
    ];

    // Set a default value for the template field.
    $templateDefaultValue = '<div class="post"><a href="{{link}}" target="_blank"><img src="{{image}}" /><div class="caption">{{caption}}</div></a></div>';
    $templateValue = isset($this->configuration['template']) ? $this->configuration['template'] : $templateDefaultValue;

    $form['template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Template'),
      '#default_value' => empty($templateValue) ? $templateDefaultValue : $templateValue,
      '#description' => $this->t('Enter the markup to use for each Instagram post. <strong>Must be on a single line.</strong> Refer to the <a target="_blank" href="https://github.com/stevenschobert/instafeed.js/wiki/Templating">template documentation</a>.<br>Default value: Displays all Instagram posts as linked images. To restore the default value, empty the field and save the form.<br>Note: Since all Instagram posts use the same template within a single block, you may want to filter the types of media if you use template code that does not apply for all types eg. video.<br>Refer to the modules README file for example template code for videos.'),
    ];

    // If the template field is empty, save the description code.
    if (empty($this->configuration['template'])) {
      $this->configuration['template'] = $templateDefaultValue;
    }

    // Disable module styling.
    $form['disable_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable module CSS'),
      '#default_value' => $this->configuration['disable_css'] ?? FALSE,
    ];

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom classes'),
      '#description' => $this->t('Optional: Enter one or more CSS classes to apply to the #instafeed div. Separate multiple classes with spaces.'),
      '#default_value' => $this->configuration['classes'] ?? '',
    ];

    return $form;
  }

  /**
   * Form element validation callback to validate the limit field.
   */
  public function validateLimit($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (!is_numeric($value) || $value < 1 || $value > 50) {
      $form_state->setError($element, $this->t('The limit must be a number between 1 and 50.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['template'] = $form_state->getValue('template');
    $this->configuration['limit'] = $form_state->getValue('limit');
    $this->configuration['filters'] = $form_state->getValue('filters');
    $this->configuration['disable_css'] = $form_state->getValue('disable_css');
    $this->configuration['classes'] = $form_state->getValue('classes');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the access token from State API.
    $access_token = $this->state->get('instafeed_block.access_token', '');

    // Retrieve block settings.
    $template = $this->configuration['template'] ?? '';
    $limit = $this->configuration['limit'] ?? 6;
    $filters = $this->configuration['filters'] ?? [];
    $disable_css = $this->configuration['disable_css'] ?? FALSE;
    $classes = $this->configuration['classes'] ?? '';

    // Conditional removal of CSS file from the library definition.
    if ($disable_css) {
      $libraries = [
        'instafeed_block/instafeed.js',
        'instafeed_block/instafeed-block-js',
      ];
    }
    else {
      $libraries = [
        'instafeed_block/instafeed.js',
        'instafeed_block/instafeed-block-js',
        'instafeed_block/instafeed-block-css',
      ];
    }

    // Add markup with custom classes if set.
    $markup = '<div id="instafeed"></div>';
    if ($classes) {
      $markup = '<div id="instafeed" class="' . $classes . '"></div>';
    }

    // Build the render array.
    $build = [
      '#markup' => $markup,
      '#attached' => [
        'library' => $libraries,
        'drupalSettings' => [
          'instafeed_block' => [
            'accessToken' => $access_token,
            'template' => $template,
            'limit' => $limit,
            'filters' => $filters,
          ],
        ],
      ],
    ];

    return $build;
  }
}
