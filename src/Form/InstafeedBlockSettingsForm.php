<?php

namespace Drupal\instafeed_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Instafeed settings form.
 */
class InstafeedBlockSettingsForm extends FormBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new InstafeedBlockSettingsForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(MessengerInterface $messenger, StateInterface $state) {
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instafeed_block_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the current access token and expiration timestamp from State API.
    $access_token = $this->state->get('instafeed_block.access_token', '');
    $expiration_timestamp = $this->state->get('instafeed_block.token_expiration', '');
    $last_execution_timestamp = $this->state->get('instafeed_block.last_execution', 0);

    // Format expiration date.
    $expiration_date = 'Will be set after first token refresh.';
    if ($expiration_timestamp) {
      $expiration_date = date('Y-m-d H:i:s', $expiration_timestamp);
    }

    // Format refresh and update date.
    $refresh_date = '';
    $update_date = '';
    if ($last_execution_timestamp) {
      $refresh_date = date('Y-m-d H:i:s', $last_execution_timestamp);
      $update_date = date('Y-m-d H:i:s', strtotime('+1 month', $last_execution_timestamp));
    }

    $form['instagram_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Instagram Access Token'),
      '#description' => $this->t('Enter your Instagram Access Token.'),
      '#default_value' => $access_token,
      '#maxlength' => 500, // Adjust this value as needed.
    );

    // Add expiration date below the token field.
    $form['instagram_token_expiration'] = array(
      '#type' => 'item',
      '#markup' => $this->t('<strong>Estimated token expiration date: @expiration_date</strong><br><small>The token expiration date is estimated based on when you add your Instagram token to the form.<br>It does not reflect the actual 2-month expiration date of the access token.<br>For example, if you create the access token a week before adding it here, the estimated expiration date will be off by one week.</small>', ['@expiration_date' => $expiration_date]),
    );

    $form['instagram_auto'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Automatic token refresh during cron'),
    );

    // Add expiration date below the token field.
    $form['instagram_auto']['instagram_auto_refresh'] = array(
      '#type' => 'item',
      '#markup' => $this->t('To enable, refer to the modules README file for instructions.<br>When enabled, last refresh and next scheduled update dates will show below after first cron run.<br><br><strong>Last refresh: @refresh_date<br>Next refresh: @update_date</strong>', ['@refresh_date' => $refresh_date, '@update_date' => $update_date]),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the access token using State API.
    $this->state->set('instafeed_block.access_token', $form_state->getValue('instagram_token'));

    // Add a success message.
    $this->messenger->addMessage($this->t('Instagram access token saved successfully.'));

    // Clear all caches.
    drupal_flush_all_caches();
  }
}
