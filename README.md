# About

The Instagram feed module provides customizable blocks that can be placed in  
any region of your website.  

It integrates the instafeed.js library  
(https://instafeedjs.com/) into your Drupal website, allowing you to display  
an Instagram feed seamlessly. Additionally, it provides functionality to  
automatically refresh the Instagram access token before it expires, ensuring  
uninterrupted access to your Instagram content.

## Requirements

This module requires the external library instafeed.js  
(https://github.com/stevenschobert/instafeed.js/releases/tag/v2.0.0) and must  
be installed so the required js file have the following url from drupal-root  
`/libraries/instafeed-js/dist/instafeed.min.js`.

You also need an Instagram access token.
Follow the guide at
https://developers.facebook.com/docs/instagram-platform/instagram-api-with-instagram-login
on how to obtain yours.

### Quick Rundown of the Steps:

- Firstly you need a businesses or creators Instagram account. If you have a
  personal account, you need to configure it to be either buisiness or creator.
  Read more about this here https://help.instagram.com/502981923235522/
- Visit the Facebook Developers page (https://developers.facebook.com/) and
log in.
- Create a Facebook app (Use case: Other) and (App type: Buisiness).
- Add the Instagram product.
- Add Instagram account and generate access token. This must be authorized with
  the same Instagram account (Only display access is required).
- Copy and securely store the access token for later use in the Instafeed  
module settings.

## Installation

Install the module as you would with any other Drupal module.  
Make sure the required library is installed correctly. If not, and error will  
be displayed on the system status page.  
Navigate to the module settings page and paste the Instagram access token  
obtained earlier.  
Finally, go to the block layout page and add the Instafeed block to the  
desired region of your website. Your Instagram feed should now be visible on  
your site. If you do not see it, try clearing the site cache.

## Block settings

The modules block settings allow you to change the markup for the  
Instagram posts, limit the number of results and choose what types of media  
you want to display. You can also disable the module CSS.

### Instagram post markup

All markup must persist on a single line.
To customize the template markup for displaying Instagram videos, you can use  
the following HTML snippet as a reference:

```html
<div class="instafeed video"><video poster="{{model.thumbnail_url}}" controls playsinline preload=auto><source src="{{model.media_url}}" type="video/mp4"></video></div>
```

For more information and additional customization options, refer to the  
Instafeed.js Templating Documentation  
(https://github.com/stevenschobert/instafeed.js/wiki/Templating).

### Limit posts

You can change the number of Instagram post you want to display using this  
filter.

### Filter media types

You can also filter what type of media types you want to include  
(Image, Video and Album).

**Limitations:** Currently only the first media item can be optained. So for  
media type Album only the first image/video in the Album will be displayed.

### Disable module CSS

The module comes with some basic CSS but this can be disabled if you want to  
implement your own styling.

## Advanced configuration (For Production Environments Only)

To enable automatic access token refreshing, modify the settings.php file with  
the following code. Ensure the URL provided does not include the protocol  
(http/https) and does not end with a slash.

```php
$settings['production_url'] = 'production-url.com';
```

When this setting is configured, the module will attempt to refresh the token  
once a month automatically.

**Important:** The production_url variable should only be set in the  
production settings.php file. This prevents development environments from  
invalidating the production token, as a valid token refresh will invalidate  
the old one. In other words, you should only enable this feature on ONE site  
only.

## Maintainers

Andreas Kleve (Kleve) (https://www.drupal.org/u/kleve)
